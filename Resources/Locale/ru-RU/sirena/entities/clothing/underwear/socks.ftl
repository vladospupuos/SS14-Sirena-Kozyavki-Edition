ent-ClothingUnderwearSocksHigh = высокие носки
    .desc = Выше вас ждут товары взамен на ваш труд.

ent-ClothingUnderwearSocksNormal = носки
    .desc = Обрадуйте Nanotrasen своей продуктивностью и заставьте эти носки вонять за 10 метров!

ent-ClothingUnderwearSocksShort = короткие носки
    .desc = По вам можно сказать, что вы аполит, раз носите такие носки. Ведь так?

ent-ClothingUnderwearSocksThigh = чулки
    .desc = Мы получим ваш труд, а вы получите условно бесплатный товар.

ent-ClothingUnderwearSocksFishnettights = чулки в сетку
    .desc = Теперь вы можете видеть свои ноги.

ent-ClothingUnderwearSocksPantyhose = колготки
    .desc = Для понастоящему серьезной работы.

ent-ClothingUnderwearSocksGarters = чулки с подвязкой
    .desc = Если вам мало просто чулков.

ent-ClothingUnderwearSocksRich = богатые чулки
    .desc = Те самые чулки, которые покажут что вы богатая сучка.