using Content.Shared.GG.Eye.NightVision.Systems;
using Robust.Shared.GameStates;

namespace Content.Shared.GG.Eye.NightVision.Components;

[RegisterComponent]
[NetworkedComponent, AutoGenerateComponentState]
[Access(typeof(NightVisionSystem))]
public sealed partial class NightVisionComponent : Component
{

    [ViewVariables(VVAccess.ReadWrite), DataField("isNightVision"), AutoNetworkedField]
    public bool IsNightVision;


    [Access(Other = AccessPermissions.ReadWriteExecute)]
    public bool DrawShadows = false;


    [Access(Other = AccessPermissions.ReadWriteExecute)]
    public bool GraceFrame = false;
}
